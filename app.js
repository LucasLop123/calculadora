const calc= require('./calculator.js'); 
const fs = require('fs');
var prompt = require('prompt'); //para invocar el prompt. sino, no puede tomar datos


function escribir(x1,x2,resultado,ope){ //función para escribir la operación en el txt (ope= +,-,x,7)

    const suma = fs.writeFileSync('log.txt',+ x1 + ' '+ ope + ' ' + x2 + ' = ' + resultado + '\n', {flag:'a'}); 
}

//para empezar el prompt(?)
prompt.start();

console.log("operaciones válidas: sumar, restar, multiplicar, dividir");

//aparentemente esta función prompt toma los valores como en una especie de array.

prompt.get(['operacion', 'primerValor', 'segundoValor'], function (err, result) {
  
  //declaración de variables (las dos tomadas del prompt) y trasnformación a enteros
  const x1=parseInt(result.primerValor);
  const x2= parseInt(result.segundoValor);

//swich dependiendo de cual sea la operación
  switch (result.operacion) {

    case "sumar":

        var ope="+"; //esto se usa solamente para darle el simbolo al writefile
        resultado=calc.sumar(x1,x2);
        console.log("el resultado de la suma es: "+ resultado);
        escribir(x1,x2,resultado,ope);//se invoca la función y se pasan las variables a la función escribir para que aparezca en el txt

        break;

    case "restar":

        var ope="-";
        resultado=calc.restar(x1,x2);
        console.log("el resultado de la resta es: "+ resultado);
        escribir(x1,x2,resultado,ope);
        
        break;

    case "multiplicar":

        var ope="x";
        resultado=calc.multiplicar(x1,x2);
        console.log("el resultado de la multiplicación es: "+ resultado);
        escribir(x1,x2,resultado,ope); 

        break;

    case "dividir":

        var ope="/";
        resultado=calc.dividir(x1,x2);
        console.log("el resultado de la división es: "+ resultado);
        escribir(x1,x2,resultado,ope);

        break;


      default:
          console.log("No es una operación válida");
          break;
  }
});
